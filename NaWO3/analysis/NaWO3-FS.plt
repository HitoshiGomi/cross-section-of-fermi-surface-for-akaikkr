## *** Graph settings ***
set pm3d map
set size square
set grid

## *** File name ***
project="NaWO3-FS"
datfile=project."_up.dat"
pngfile=project."_up.png"
epsfile=project."_up.eps"

## *** Plot range ***
stats datfile
xmax=STATS_pos_max_y
ymax=STATS_max_y
set xrange [0:xmax]
set yrange [0:ymax]

## *** Axis settings ***
set xtics ("{M}" 0, "{X}" 0.5*xmax, "{M}" xmax)
set ytics ("{M}" 0, "{X}" 0.5*ymax, "{M}" ymax)
unset colorbox

## *** Color settings ***
set palette defined (0 "white", 1 "red")

## *** Plot ***
splot datfile notitle

## *** Image output ***
set terminal pngcairo size 520,520
set output pngfile
rep
# set terminal postscript enhanced color font "Arial"
# set output epsfile
# rep