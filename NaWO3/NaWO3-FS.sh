#!/bin/csh -f

# ***************************************************************************************
# Last modified 2019/01/05
#
#
# *** Sample files ***
#   NaWO3
#    |-- NaWO3-FS.sh
#    |-- analysis
#    |   `-- NaWO3-FS.plt
#    |-- data
#    |-- in
#    |   |-- NaWO3-Gamma.in
#    |   `-- NaWO3.in
#    |-- out
#    `-- template
#        `-- NaWO3-FS_Template.in
#
#
# *** Usage ***
#     0. This script requires sed, awk, tcsh and AkaiKKR.
#     1. Run "go" calculation as usual (in/NaWO3.in).
#     2. Make an excutable "specxFS" (see below).
#     3. Edit (if needed) and run this script.
#     4. Plot the dat file generated in analysis/ (see also analysis/NaWO3-FS.plt).
#
#
# # *** specxFS ***
# 1. Backup specx
#     mv specx specx.back
# 2. Edit the following source files
#     * source/specx.f
#          msex/2/
#     * source/cemesr.f
#          data ref/0.5d0/
#     * source/spmain.f
#          irdfmt=1
#          iwrtfmt=1
# 2. make
# 3. Rename
#     mv specx specxFS
# 4. Restore the specx
#     mv specx.back specx
# 5. Restore the source files
# 6. Run test calculation (in/NaWO3-Gamma.in)
#
#
# *** Note ***
# Please cite the following paper.
#     Electrical resistivity of substitutionally disordered hcp Fe-Si and Fe-Ni alloys: 
#     Chemically-induced resistivity saturation in the Earth's core
#     Hitoshi Gomi, Kei Hirose, Hisazumi Akai, Yigwei Fei
#     Earth and Planetary Science Letters (2016), Volume 451, 1 October 2016, Pages 51-61
#     http://dx.doi.org/10.1016/j.epsl.2016.07.011
#
# ***************************************************************************************


# ***************************************************************************************
# *                                                                                     *
# *                                Please edit FROM here.                               *
# *                                                                                     *
# ***************************************************************************************
## Lattice parameters (Bohr)
set a=7.5
set boa=1
set coa=1

## Energy level
set energy=0.0010000

## Execute file name
set execfs="specxFS"
#set execfs="~/kkr/cpa2002v010/specxFS"

## File names
## Potential file name
set potential=NaWO3
# Project name
set project=${potential}-FS
# Template file name
set template=${project}_Template.in

## The plane of the cross section difined by PA and PB vectors
#
#        B(bx, by, bz)
#            ______________
#           ^              /
#          /              /
#         /              /
#        /              /
#       /              /
#      /              /
#     .--------------> A(ax, ay, az)
#  P(px, py, pz)
#
# *** A(ax, ay, az) ***
set ax=0.5
set ay=-0.5
set az=0
# *** B(bx, by, bz) ***
set bx=-0.5
set by=0.5
set bz=0
# *** P(px, py, pz) ***
set px=-0.5
set py=-0.5
set pz=0

# Resolution of PA vector
set n=255
# Resolution of PB vector
set m=255
# ***************************************************************************************
# *                                                                                     *
# *                                Please edit UNTIL here.                              *
# *                                                                                     *
# ***************************************************************************************




# *** Backup the result of previous calculation ***
if ( -e analysis/${project}_up.dat ) then
  mv analysis/${project}_up.dat analysis/${project}_up.dat.back
endif
if ( -e analysis/${project}_dn.dat ) then
  mv analysis/${project}_dn.dat analysis/${project}_dn.dat.back
endif

### ********************* For GNUPLOT **********************
## Shift
# A(akx, aky, akz)
set pi=`echo "4*a(1)" | bc -l`	
set akx=`echo "(${ax}-(${px}))*${pi}/${a}" | bc -l`
set aky=`echo "(${ay}-(${py}))*${pi}/(${boa}*${a})" | bc -l`
set akz=`echo "(${az}-(${pz}))*${pi}/(${coa}*${a})" | bc -l`
# B(bkx, bky, bkz)
set bkx=`echo "(${bx}-(${px}))*${pi}/${a}" | bc -l`
set bky=`echo "(${by}-(${py}))*${pi}/(${boa}*${a})" | bc -l`
set bkz=`echo "(${bz}-(${pz}))*${pi}/(${coa}*${a})" | bc -l`

## Rotation about z-axis
## A(akx, aky, akz) --> A'(akdx, 0,    akdz)
## B(bkx, bky, bkz) --> B'(bkdx, bkdy, bkdz)
set costh1=`echo "${akx} / sqrt((${akx}^2)+(${aky}^2))" | bc -l`
set sinth1=`echo "${aky} / sqrt((${akx}^2)+(${aky}^2))" | bc -l`
# A'(akdx, 0, akdz)
set akdx=`echo "(${akx})*${costh1}+(${aky})*${sinth1}" | bc -l`
set akdy=0
set akdz=${akz}
# B'(bkdx, bkdy, bkdz)
set bkdx=`echo "(${bkx})*${costh1}+(${bky})*${sinth1}" | bc -l`
set bkdy=`echo "-1*(${bkx})*${sinth1}+(${bky})*${costh1}" | bc -l`
set bkdz=${bkz}

## Rotation about y-axis
## A'(akdx, 0,    akdz) --> A''(akddx, 0,     0)
## B'(bkdx, bkdy, bkdz) --> B''(bkddx, bkddy, bkddz)
set costh2=`echo "${akdx} / sqrt((${akdx}^2)+(${akdz}^2))" | bc -l`
set sinth2=`echo "${akdz} / sqrt((${akdx}^2)+(${akdz}^2))" | bc -l`
# A''(akddx, 0, 0)
set akddx=`echo "(${akdx})*${costh2}+(${akdz})*${sinth2}" | bc -l`
set akddy=0
set akddz=0
# B''(bkddx, bkddy, bkddz)
set bkddx=`echo "(${bkdx})*${costh2}+(${bkdz})*${sinth2}" | bc -l`
set bkddy=${bkdy}
set bkddz=`echo "-1*(${bkdx})*${sinth2}+(${bkdz})*${costh2}" | bc -l`

## Rotation about z-axis
## A''(akddx, 0,     0)     --> A'''(akdddx, 0,     0)
## B''(bkddx, bkddy, bkddz) --> B''(bkdddx, bkdddy, 0)
set costh3=`echo "${bkddy} / sqrt((${bkddy}^2)+(${bkddz}^2))" | bc -l`
set sinth3=`echo "${bkddz} / sqrt((${bkddy}^2)+(${bkddz}^2))" | bc -l`
# A'''(akdddx, 0, 0)
set akdddx=${akddx}
set akdddy=0
set akdddz=0
# B'''(bkdddx, bkdddy, 0)
set bkdddx=${bkddx}
set bkdddy=`echo "(${bkddy})*${costh3}+(${bkddz})*${sinth3}" | bc -l`
set bkdddz=0
### ********************* For GNUPLOT **********************



### ********************* For AkaiKKR **********************
set i=0
while ( $i <= $n )
  # File names
  set num=` printf "%03d\n" ${i}`
  sed 's/'POTENTIAL'/'${potential}'/g' template/${template} > in/${project}_${num}.in
  set j=0
  while ( $j <= $m )
    ## Q vector
    set qx=`echo "${px}+(${i}/${n})*(${ax}-(${px}))+(${j}/${m})*(${bx}-(${px}))" | bc -l`
    set qy=`echo "${py}+(${i}/${n})*(${ay}-(${py}))+(${j}/${m})*(${by}-(${py}))" | bc -l`
    set qz=`echo "${pz}+(${i}/${n})*(${az}-(${pz}))+(${j}/${m})*(${bz}-(${pz}))" | bc -l`
    printf "%1.8f %1.8f %1.8f \n" $qx $qy $qz >> in/${project}_${num}.in
    @ j ++
  end

  ## Execute the AkaiKKR
  ${execfs} < in/${project}_${num}.in > out/${project}_${num}.out
  mv data/${potential}_up.spc data/${potential}_${num}_up.spc
  mv data/${potential}_dn.spc data/${potential}_${num}_dn.spc
  ## Extract the Bloch-Spectral function in the vicinity of the Fermi level
  cat data/${potential}_${num}_up.spc | awk '$2=='"${energy}"'{print $3}' > analysis/${potential}_up.tmp
  cat data/${potential}_${num}_dn.spc | awk '$2=='"${energy}"'{print $3}' > analysis/${potential}_dn.tmp
  set j=0
  while ( $j <= $m )
     set qkx=`echo "((${i}/${n})*${akdddx}+(${j}/${m})*${bkdddx})" | bc -l`
     set qky=`echo "((${i}/${n})*${akdddy}+(${j}/${m})*${bkdddy})" | bc -l`
     set qkz=`echo "((${i}/${n})*${akdddz}+(${j}/${m})*${bkdddz})" | bc -l`

     set qkx=`printf "%1.8f" ${qkx}`
     set qky=`printf "%1.8f" ${qky}`
     set qkz=`printf "%1.8f" ${qkz}`

     @ j ++

     sed -i -e ''"${j}"'s/^/'${qkx}\ ${qky}\ '/g' analysis/${potential}_up.tmp
     sed -i -e ''"${j}"'s/^/'${qkx}\ ${qky}\ '/g' analysis/${potential}_dn.tmp
  end
  cat analysis/${potential}_up.tmp >> analysis/${project}_up.dat
  cat analysis/${potential}_dn.tmp >> analysis/${project}_dn.dat
  printf "\n" >> analysis/${project}_up.dat
  printf "\n" >> analysis/${project}_dn.dat
 @ i ++
end
### ********************* For AkaiKKR **********************



## Delete the temporary files
rm analysis/${potential}_up.tmp
rm analysis/${potential}_dn.tmp
# rm in/${project}_*.in
# rm out/${project}_*.out
# rm data/${potential}_*_up.spc
# rm data/${potential}_*_dn.spc
