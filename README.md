# Description
This project contains a C shell script and sample files, which help researchers to plot the cross-section of the Fermi surface (Bloch spectral function) calculated by using the AkaiKKR DFT package (<http://kkr.issp.u-tokyo.ac.jp>).

# Requirements
* tcsh
* sed
* awk
* gnuplot (Version 4.6 or later)
* AkaiKKR <http://kkr.issp.u-tokyo.ac.jp> (Version August 22, 2018 or later)

# Preparation
In order to use this script, you have to install the required packages listed above.
Additionally, you have to make the AkaiKKR excutable.

0. Clone this project
```
git clone https://gitlab.com/HitoshiGomi/cross-section-of-fermi-surface-for-akaikkr.git
```

1. Backup specx

Go to the AkaiKKR directory (e.g. ~/kkr/cpa2002v010/), and backup the original specx excutable.
```
mv specx specx.back
```

2. Edit the following the AkaiKKR source files.
* source/specx.f: msex/2/
* source/cemesr.f: data ref/0.5d0/
* source/spmain.f: irdfmt=1, iwrtfmt=1

3. make.
```
make
```

4. Rename the excutable and restore the original specx.
```
mv specx specxFS
mv specx.back specx
```

5. Restore the source files

6. Run the test calculation (in/NaWO3-Gamma.in)
```
specxFS < in/NaWO3-Gamma.in > out/NaWO3-Gamma.out
```


# Tutorial for the NaWO3 sample
This project contain the sample files, which calculate the cross-section of the Fermi surface of NaWO3.
The other samples may be found in the wiki.

```
  NaWO3  
   |-- NaWO3-FS.sh  
   |-- analysis  
   |   `-- NaWO3-FS.plt  
   |-- data  
   |-- in  
   |   |-- NaWO3-Gamma.in  
   |   `-- NaWO3.in  
   |-- out  
   `-- template  
       `-- NaWO3-FS_Template.in  
```

1. Run "go" calculation as usual (in/NaWO3.in).
```
specx < in/NaWO3.in > out/NaWO3.out
```

2. Edit (if needed) and run the NaWO3.sh.
```
chmod +x NaWO3-FS.sh
./NaWO3-FS.sh
```

3. Plot the dat file by using the gnuplot (see also analysis/NaWO3-FS.plt).
```
cd analysis/
gnuplot "NaWO3-FS.plt"
```


# License
This project is provided under the MIT license (see LICENSE file).

Also, I am grateful if you cite the following paper.  
Electrical resistivity of substitutionally disordered hcp Fe-Si and Fe-Ni alloys: Chemically-induced resistivity saturation in the Earth's core  
Hitoshi Gomi, Kei Hirose, Hisazumi Akai, Yigwei Fei  
Earth and Planetary Science Letters (2016), Volume 451, 1 October 2016, Pages 51-61  
<http://dx.doi.org/10.1016/j.epsl.2016.07.011>
